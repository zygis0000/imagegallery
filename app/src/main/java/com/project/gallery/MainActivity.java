package com.project.gallery;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;

import android.widget.GridView;

import com.project.gallery.entities.Image;
import com.project.gallery.tools.ImageAdapter;
import com.project.gallery.tools.JsonParser;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private GridView view;
    private ImageAdapter imageAdapter;
    private List<Image> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JsonParser parser = new JsonParser(getApplicationContext());
        view = (GridView) findViewById(R.id.gridView);
        try {
            data = parser.parseJsonToArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        imageAdapter = new ImageAdapter(this, R.layout.grid_item_layout, data);
        view.setAdapter(imageAdapter);


    }
}
