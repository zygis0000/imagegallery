package com.project.gallery.tools;

import android.app.Activity;
import android.content.Context;

import com.jayway.jsonpath.JsonPath;
import com.project.gallery.entities.Image;
import com.project.gallery.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JsonParser extends Activity {
    private static Context context;

    public JsonParser(Context context) {
        this.context = context;
    }

    private String getUrlInString() throws IOException {

        InputStream stream = context.getResources().openRawResource(R.raw.imagejson);
        BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = rd.readLine()) != null) {
            response.append(inputLine);
        }
        rd.close();
        return response.toString();
    }

    public List<Image> parseJsonToArray() throws Exception {
        String json = getUrlInString();
        int lenght = JsonPath.read(json, "$.gallery.length()");
        List<Image> imageUrls = new ArrayList<>();
        for (int i = 0; i < lenght; i++) {
            Image image = new Image();
            image.setImage((String) JsonPath.read(json, "$.gallery[" + i + "].url"));
            imageUrls.add(image);
        }
        return imageUrls;
    }
}
