package com.project.gallery.tools;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


import com.project.gallery.entities.Image;
import com.project.gallery.R;
import com.squareup.picasso.Picasso;


import java.util.List;

public class ImageAdapter extends ArrayAdapter<Image> {
    private Context context;
    private int resourceId;
    private List<Image> data;

    public ImageAdapter(Context context, int resourceId, List<Image> mGridData) {
        super(context, resourceId, mGridData);
        this.resourceId = resourceId;
        this.context = context;
        this.data = mGridData;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resourceId, parent, false);
            holder = new ViewHolder();
            holder.imageView = row.findViewById(R.id.iv_Grid);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Image image = data.get(position);
        Picasso.with(context).load(image.getImage()).into(holder.imageView);
        return row;
    }

    static class ViewHolder {
        ImageView imageView;
    }
}

